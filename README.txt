
Instalando en MOODLE

1.- Crear los campos necesarios en MOODLE para que se puedan ingresar las cuentas de las redes sociales de los alumnos matriculados. Esto se logra accediendo al menú: \textbf{Administración $>$ Administración del Sitio $>$ Usuarios $>$ Cuentas $>$ Campos de perfil del usuario.} La descripción de la campo se sugiere que sea en el caso de Twitter, ``Cuenta de Twitter'' y forzosamente el campo deberá llamarse ``Twitter". En el caso de Facebook, ``Cuenta de Facebook y forzosamente el campo deberá llamarse ``Facebook". Porque es así como los detecta el \textit{plugin}. 

2.- Descomprimir la carpeta  en la ruta de archivos de MOODLE: \textbf{*/mod.}

3.- Acceder a MOODLE normalmente (como administrador) y deberá de ser detectado. De no ser así, se ingresa a Administración del Sitio -> Notificaciones.

4.- Dar clic en "Actualizar base de datos Moodle ahora".

El plugin podrá ser usado sin problemas. Sólo hay que solicitar a los alumnos matriculados que ingresen sus cuentas de Twitter o Facebook. 