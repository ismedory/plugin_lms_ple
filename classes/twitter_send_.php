<?php

/**
 * Notifier for twitter used in newevents
 *
 * @package    mod_newevents
 * @copyright  2014 ISMERAÍ RUBÍ GARCÍA ROMÁN <ismerai.groman@gmail.com>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace mod_newevents;
require_once($CFG->dirroot . '/mod/newevents/classes/notifier.php');
defined('MOODLE_INTERNAL') || die();


/**
 * Event observer for mod_newevents.
 */
 
class twitter_send extends notifier{

	public function send($data){
		global $CFG, $DB;
		require_once $CFG->dirroot .'\mod\newevents\twitterOauth\twitteroauth.php';
		//require_once 'twitteroauth.php';
		
		$assignname = $DB->get_field('assign','name', array('id' => $data->assign));
		$coursename = $DB->get_field('course', 'fullname', array('id' => $data->course));
		
		//Obtener el ID del campo de Twitter
		$idtwitter = $DB->get_field('user_info_field', 'id', array('shortname' => "Twitter"));
		//Obtener la cuenta de Twitter del Usuario
		$twitter = $DB->get_field('user_info_data', 'data', array('fieldid' => $idtwitter));
		
		
		define("CONSUMER_KEY", "Imda1Sl1J7ugkB1E9qCirSVXZ");
		define("CONSUMER_SECRET", "5mZVFYCTT4Yzhtmq7Edr4YSu5am8nwyeLoFPwk8FSvT6RzLh6H");
		define("OAUTH_TOKEN", "39917656-9LrRUPERfMa08gpPQsQewkM9M4RbAEItqihQGYwIi");
		define("OAUTH_SECRET", "46nLGxF8YM8TBtCSgVv9fZgl6ORmDYFQukCqVHbcrHU44");
		$connection = new \TwitterOAuth(CONSUMER_KEY, CONSUMER_SECRET, OAUTH_TOKEN, OAUTH_SECRET);
		$content = $connection->get('account/verify_credentials');
		
		$mensaje = 'mensaje '. $data->idnotification . '": "'.$twitter. '" te han calificado la tarea "' . $assignname . '" de tu curso "' . $coursename . '"';
		
		
		$connection->post('statuses/update', array('status' => $mensaje));
	}

}