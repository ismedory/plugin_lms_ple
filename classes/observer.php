<?php

/**
 * event observers used in newevents
 *
 * @package    mod_newevents
 * @copyright  2014 ISMERAÍ RUBÍ GARCÍA ROMÁN <ismerai.groman@gmail.com>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
namespace mod_newevents;

defined('MOODLE_INTERNAL') || die();

/**
 * event observer for mod_newevents.
 */
 
global $CFG;	
    	# print_r($CFG);
    	require_once $CFG->dirroot . '/mod/newevents/classes/facebook_send.php';
		require_once $CFG->dirroot . '/mod/newevents/classes/twitter_send.php';
		
class observer {

    /**
     * Se envía una notificación
     *
     * @param $eventdata
     */
    static $idnotification;
    
    public static function notification_send($event) {
    	global $CFG;	
    	# print_r($CFG);
    	global $DB;
		
		//Obteniendo el id del curso...
        $assign = $DB->get_field('assign_grades','assignment', array('id' => $event->itemid));
		$course = $DB->get_field('assign', 'course', array('id' => $assign));
	    $event->course = $course;
		$event->assign = $assign;
	    self::add_db($event);
		$event->idnotification = self::$idnotification;
		
		
		//Enviando notificación al PLE..		
		$obj = new twitter_send();
		$obj->send($event);
		
    }
	
	//Función que agregará los datos a la base de datos
	public static function add_db($eventdata){
		 	
	    global $DB;
		
		
		
		//Insertando los datos en la tabla eventos
		$evento = array(
			'tipo' => 1,
			'course' => (int)$eventdata->course,
			'fecha_creacion' => time()
		);
		$evento = json_decode(json_encode($evento));
		$idevent = $DB->insert_record('eventos', $evento);
	    
		//Insertando los datos en la tabla notificaciones
		$notificacion = array(
			'userid' => $eventdata->user,
			'fecha_envio' => time(),
			'enviado' => 1,
			'evento' => $idevent
		);
		$notificacion = json_decode(json_encode($notificacion));
		self::$idnotification = $DB->insert_record('notificaciones', $notificacion);
		
	}


}
