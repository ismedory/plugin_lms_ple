<?php

/**
 * Notifier used in newevents
 *
 * @package    mod_newevents
 * @copyright  2014 ISMERAÍ RUBÍ GARCÍA ROMÁN <ismerai.groman@gmail.com>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
namespace mod_newevents;

defined('MOODLE_INTERNAL') || die();

/**
 * Event observer for mod_newevents.
 */
abstract class notifier {
	
	public abstract function send($params);

}