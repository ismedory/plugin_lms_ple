<?php
   
 /**
 * Notifier for facebook used in newevents
 *
 * @package    mod_newevents
 * @copyright  2014 ISMERAÍ RUBÍ GARCÍA ROMÁN <ismerai.groman@gmail.com>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
namespace mod_newevents;
require_once($CFG->dirroot . '/mod/newevents/classes/notifier.php');
defined('MOODLE_INTERNAL') || die();


/**
 * Event observer for mod_newevents.
 */
class facebook_send extends notifier {
	
	
	public function send($data){
		global $CFG, $DB;
		require_once $CFG->dirroot .'\mod\newevents\facebook\src\facebook.php';

		$assignname = $DB->get_field('assign','name', array('id' => $data->assign));
		$coursename = $DB->get_field('course', 'fullname', array('id' => $data->course));

		$app_id     = '447553182016253'; //  ID de nuestra aplicación
		$app_secret = '2bb5d51ca98ef81267666a7a4dbcaf1a'; //  Secret de nuestra aplicación
		$token = 'CAAGXDBhuov0BADOj6WiapT6qHeniigZCjwXzOlI4Q73AWJmiVdokfl7YOG6PcpHeQgZCnq5bl9hS3TtgiMVzndYy6e2vjaGWVSopHZC9PJxrVz0osangCR2mXAZCkf2VemOLLJrzZB83fTJGI4eIqsQEigX0ZCKOMxebAx1joWDoItBobbiEjyWAdqASZBGtVAZD'; // ponemos nuestro token
		
		$facebook = new \Facebook(array(
		    'appId' => $app_id,
		    'secret' => $app_secret,
		    'cookie' => false
		));
		$req =  array(
		    'access_token' => $token,
		    'message' => 'mensaje '. $data->idnotification . ': te han calificado la tarea "' . $assignname . '" de tu curso "' . $coursename . '"');
			
			$destino = "ismedory";
			
			
		$res = $facebook->api('/301432683348230/feed', 'POST', $req
		);
				
		
		}
	
}