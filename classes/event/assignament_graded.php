<?php


/**
 * Newevents module event class.
 *
 * @package    mod_newevents
 * @copyright  2014 ISMERAÍ RUBÍ GARCÍA ROMÁN <ismerai.groman@gmail.com>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
namespace mod_newevents\event;
defined('MOODLE_INTERNAL') || die();

/**
 * Event for when a new event is created (example: graded assignament).
 *
 * @package    mod_newevents
 * @copyright  2014 ISMERAÍ RUBÍ GARCÍA ROMÁN <ismerai.groman@gmail.com>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class assignament_graded extends \core\event\base {
	public $user, $itemid, $grade, $assign, $course, $idnotification;

	protected function init(){
		$this->data['crud'] = 'u';
        $this->data['level'] = self::LEVEL_TEACHING;
        $this->data['objecttable'] = 'assign';
		
	}
}
	