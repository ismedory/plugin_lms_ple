<?php

/**
 * Notifier for twitter used in newevents
 *
 * @package    mod_newevents
 * @copyright  2014 ISMERAÍ RUBÍ GARCÍA ROMÁN <ismerai.groman@gmail.com>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace mod_newevents;
require_once($CFG->dirroot . '/mod/newevents/classes/notifier.php');
defined('MOODLE_INTERNAL') || die();


/**
 * Event observer for mod_newevents.
 */
 
class twitter_send extends notifier{

	public function send($data){
		global $CFG, $DB;
		require_once $CFG->dirroot .'\mod\newevents\twitterOauth\twitteroauth.php';
		//require_once 'twitteroauth.php';
		
		$assignname = $DB->get_field('assign','name', array('id' => $data->assign));
		$coursename = $DB->get_field('course', 'fullname', array('id' => $data->course));
		
		//Obtener el ID del campo de Twitter
		$idtwitter = $DB->get_field('user_info_field', 'id', array('shortname' => "Twitter"));
		//Obtener la cuenta de Twitter del Usuario
		$twitter = $DB->get_field('user_info_data', 'data', array('fieldid' => $idtwitter, 'userid' => $data->user));
		
		define("CONSUMER_KEY", "T7q5khNJX5NhpbFEBk0qseKUy");
		define("CONSUMER_SECRET", "dBe5W8fW5A41cvnWHSC6ugDF6JVQ4FzO3q35qB8bS6ERg8tq6x");
		define("OAUTH_TOKEN", "2608708951-V2eRhtG5AalxjvYHxNOK91bd4UNETbtjnF16iQr");
		define("OAUTH_SECRET", "RZzU4hHjdXLfkFGwwxmglvCqNeW24CD5KsWaEfay2N8mx");
		$connection = new \TwitterOAuth(CONSUMER_KEY, CONSUMER_SECRET, OAUTH_TOKEN, OAUTH_SECRET);
		$content = $connection->get('account/verify_credentials');
		
		$mensaje = 'mensaje '. $data->idnotification . ': '.$twitter. ' te han calificado la tarea "' . $assignname . '" de tu curso "' . $coursename . '"';		
		
		$connection->post('statuses/update', array('status' => $mensaje));
	}

}