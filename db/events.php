<?php
/*
The observers are described in db/events.php in the array $observers, 
the array is not indexed and contains a list of observers defined as an array with the following properties;

eventname - event class name or "*" indicating all events. All events must use namespace, ex.: \plugintype_pluginname\event\something_happened.
callback - PHP callable type.
includefile - optional. File to be included before calling the observer. Path relative to dirroot.
priority - optional. Defaults to 0. Observers with higher priority are notified first.
internal - optional. Defaults to true. Non-internal observers are not called during database transactions, but instead after a successful commit of the transaction.

$observers = array(
 
    array(
        'eventname'   => '\core\event\sample_executed',
        'callback'    => 'core_event_sample_observer::observe_one',
    ),
 
    array(
        'eventname'   => '\core\event\sample_executed',
        'callback'    => 'core_event_sample_observer::external_observer',
        'priority'    => 200,
        'internal'    => false,
    ),
 
    array(
        'eventname'   => '*',
        'callback'    => 'core_event_sample_observer::observe_all',
        'includefile' => null,
        'internal'    => true,
        'priority'    => 9999,
    ),
 
);

*/


/**
 * Add event handlers for the newevents
 *
 * @package    mod_newevents
 * @category   event
 * @copyright  2014 ISMERAÍ RUBÍ GARCÍA ROMÁN
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */


defined('MOODLE_INTERNAL') || die();

$observers = array(

    array(
        'eventname' => '\mod_newevents\event\assignament_graded',
        'callback' => '\mod_newevents\observer::notification_send',
       
    )
);
